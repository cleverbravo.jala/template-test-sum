# template-test-sum
To run the script we need:

    * dotnet 
    * PowerShell
    * Git 
    * Some application to open a csv file


## Getting started
```
.\run.ps1
```

## The general idea

1.- Define the problem set

2.- For each problem, create two templates, 
	one for solving the problem.
	and the other for testing the answer.

3.- Create the repositories for the applicants

	Applicant	Git repository
	ABC		gitlab.com/abc-answer1
	DEF		gitlab.com/def-answer1

4.- Wait for the answers

5.- And run the script 

## Some additional ideas

Run all of this in docker.

Use some data source to handle the problem set and the applicants
