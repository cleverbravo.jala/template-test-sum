# we can get this data from a data source or something like that
$applicant="abc-sum-answer"
cd ..

mkdir $applicant
cd $applicant
git clone https://gitlab.com/cleverbravo.jala/$applicant
git clone https://gitlab.com/cleverbravo.jala/template-test-sum

cp -Recurse -Verbose .\template-test-sum\test.sum\ .\$applicant\

cd .\$applicant\test.sum\
dotnet test

cp .\bin\Debug\net7.0\postulante-b.csv ..\..\$applicant.csv
Invoke-Item ..\..\$applicant.csv
